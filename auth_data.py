#!/usr/bin/python2
import sys

def auth_data(ip_adr=None):
    """Find username and password by IP adress - mock"""
    if ip_adr is not None:
        return 'nikolay', '12345678'  # Guess there must be some db check
    else:
        print >>sys.stderr, 'Plz give some IP!'
        raise ValueError

if __name__ == '__main__':
    print >>sys.stderr, "Can't run this as stand-alone module"
    sys.exit(2)
