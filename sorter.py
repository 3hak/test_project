#!/usr/bin/python2
"""
@author: Nikolay Z.
Created 2017/07/09
"""
import sys
import logging
from multiprocessing.dummy import Pool
from auth_data import auth_data
from client import find_log_files, search_in_file


THREAD_NUMBER = 2  # Define required threadpool (also can be moved to stdin)

logging.basicConfig(level=logging.DEBUG)


class WrongInput(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            ip_adr, log_mask, numeral_id = sys.argv[1:]
            login, password = auth_data(ip_adr=ip_adr)
            logging.debug("Params is: {} {} {}".format(ip_adr, log_mask, numeral_id))
        except ValueError:
            raise WrongInput("Not enough values in input!")

        files_list = find_log_files(ip_adr, login, password, log_mask)
        logging.debug(files_list)

        # Some multithreadint (in case if there are many files w/ logs on our server)
        pool = Pool(THREAD_NUMBER)
        found_lines = pool.map(search_in_file, [(numeral_id, file_from_list, ip_adr, login, password) for file_from_list in files_list])
        # Let's make them flat to more readability in console output
        flat_lines = [item for subresult in found_lines for item in subresult]

        print 'Result:'
        for line in flat_lines: print line  # Actually result
        return 0


    except WrongInput, err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, "Wrong input! Expect: IP LogMask NumID"
        return 2

if __name__ == "__main__":
    sys.exit(main())
