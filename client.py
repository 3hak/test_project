#!/usr/bin/python2

import logging
import sys
from paramiko.client import SSHClient, AutoAddPolicy

COMMAND_FIND = "find {} -name '{}'"
SERVER_FILEPATH = '/home/nikolay/sample_logs'  # Define server file path


def search_in_file(list_of_args):
    """Convert `f([1,2, ...])` to `f(1,2, ...)` call. Miserable try to replace starmap() from Python3"""
    return search_in_file_impl(*list_of_args)


def get_client(ip_adr, login, password):
    """Auxilary method to get SSHClient. Moved out to avoid copy-paste"""
    client = SSHClient()
    client.set_missing_host_key_policy(AutoAddPolicy())
    client.connect(hostname=ip_adr, username=login, password=password)
    return client


def find_log_files(ip_adr, login, password, log_mask, client=None):
    """Find on server all files that fit given mask"""
    client = client or get_client(ip_adr, login, password)
    ssh_stdin, ssh_stdout, ssh_stderr = client.exec_command(COMMAND_FIND.format(SERVER_FILEPATH, log_mask), timeout=3)  # @UnusedVariable
    files_list = ssh_stdout.read()
    client.close()
    return files_list.strip().split('\n')


def search_in_file_impl(ident, filename, ip_adr, login, password):
    """Find lines that matches ident in given file on remote server"""
    client = get_client(ip_adr, login, password)
    line_number = 0
    result = []
    sftp_client = client.open_sftp()
    remote_file = sftp_client.open(filename)
    try:
        for line in remote_file:  # According to docs, this line-by-line read is really fast!
            if ident in line:
                shadow_copy_client = sftp_client.open(filename)  # Read all file content only if match was found
                shadow_copy = shadow_copy_client.read().split('\n')
                begin_index = 0 if line_number < 100 else line_number - 100  # To avoid slice goes mad with negative indexes
                result += shadow_copy[begin_index:line_number+101]  # No fall back: end index is safe to be more than len(list)
                shadow_copy_client.close()
            else:
                line_number+=1
    finally:
        remote_file.close()
        client.close()
        logging.debug(result)
        return result


if __name__ == '__main__':
    print >>sys.stderr, "Can't run this as stand-alone module"
    sys.exit(2)
