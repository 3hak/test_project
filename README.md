# README #

Simple test project. Python 2.7

### What it does? ###

* Takes IP of remote server, log mask and NumeralIdent via stdin 
* Connects on remote server (via mocked w/ auth\_data credentials
* Finds all log that complient given file mask
* Prints all lines w/ NumeralIdent from logs +100 lines in both directions

### Does it work? ###
* Yes, it does! Python 2.7 on Ubuntu 14.04 (VirtualBox machine on Win host)
* auth\_data is mock, it returns login and password for given IP